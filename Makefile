SHELL=/bin/bash
.PHONY: clean build start log shell all venv

all:

MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
CURRENT_PATH := $(dir $(MKFILE_PATH))
CURRENT_DIR := $(notdir $(patsubst %/,%,$(dir $(MKFILE_PATH))))

PROJECT_NAME = core
PROJECT_USER := $(PROJECT_NAME)
PROJECT_DEPLOY_ENV = development
PROJECT_FULL_NAME := $(PROJECT_NAME)_$(PROJECT_DEPLOY_ENV)
PROJECT_DOMAIN = sourosh.ahmadly.com
PROJECT_IPADDR = 127.0.0.1
PROJECT_PORT_HTTP = 51080
PROJECT_PORT_HTTPS = 51443
PROJECT_PORT_UWSGI = 51081
PROJECT_PORT_API = 51082
PROJECT_ROOT := $(CURRENT_PATH:%/=%)
PROJECT_SRC_DIR = src
PROJECT_SRC := $(PROJECT_ROOT)/$(PROJECT_SRC_DIR)
PROJECT_VENV_DIR = venv
PROJECT_VENV := $(PROJECT_ROOT)/$(PROJECT_VENV_DIR)
PROJECT_DEPLOY_DIR = deploy
PROJECT_DEPLOY := $(PROJECT_ROOT)/$(PROJECT_DEPLOY_DIR)
PROJECT_LOG_DIR := $(PROJECT_ROOT)/log
PROJECT_TMP_DIR = /tmp
PROJECT_SPHINX_DIR = docs
PYTHON2 = python2
PYTHON3 = python3
PYTHON_OPTS = -u -B -W once
PYTHON := $(PYTHON3) $(PYTHON_OPTS)

ifeq ($(PROJECT_DEPLOY_ENV),development)
DB_USER:=mysql1
DB_PASS:=mysql1
DB_NAME:=mysql1

RABBITMQ_USER:=rabbitmq
RABBITMQ_PASS:=rabbitmq
else
DB_USER:=sourosh_user
DB_PASS:=br8rBu4HJmptGF4b
DB_NAME:=sourosh_db

RABBITMQ_USER:=$(PROJECT_USER)
RABBITMQ_PASS:=u4HJmptbr8rBGF4b
endif


install: ubuntu.install.update \
		ubuntu.install.package \
		broker.rabbitmq.setup \
		ubuntu.setup.user \
		ubuntu.setup.locale \
		ubuntu.setup.time \
		ubuntu.setup.hostname \
		ubuntu.setup.log \
		ubuntu.setup.ulimit \
		ubuntu.fixperm \
		db.mysql.setup \
		webserver.nginx.setup \
		ubuntu.setup.systemd \
		django.setup


docker.compose.up: docker.compose.stop docker.compose.build
	docker-compose up --detach --remove-orphans

docker.compose.stop:
	docker-compose stop --timeout 10

docker.compose.log:
	docker-compose logs --follow --tail="all"

docker.compose.pull:
	docker-compose pull

docker.compose.build:
	docker-compose build --pull $(build.args)

docker.compose.clean: docker.compose.stop
	docker-compose rm   --force --stop -v
	docker-compose down --remove-orphans
	docker system prune -a

ubuntu.install.update:
	apt update
	apt full-upgrade -y -qq


ubuntu.install.package:
	apt install -y -qq  build-essential \
						python3-dev python3-pip python3-venv \
						bash-completion command-not-found-data command-not-found \
						geoip-database geoip-database-extra \
						htop make nano vim tmux ranger curl wget rsync gettext

	apt-get clean

ubuntu.setup.user:
	id -u $(PROJECT_USER) &>/dev/null || useradd -m $(PROJECT_USER)
	usermod -aG sudo $(PROJECT_USER)
	echo "$(PROJECT_USER) ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/$(PROJECT_USER)

ubuntu.setup.locale:
	locale-gen en_US.UTF-8
	locale-gen fa_IR
	localectl set-locale LANG=en_US.utf8
	localectl set-locale LANGUAGE=en_US.utf8

ubuntu.setup.time:
	timedatectl set-timezone Asia/Tehran
	timedatectl set-local-rtc false
	timedatectl set-ntp true

ubuntu.setup.hostname:
	echo hostnamectl set-hostname $(PROJECT_DOMAIN)


ubuntu.setup.firewall:
	apt install -y -qq ufw
	ufw allow 1001
	ufw enable
	ufw status

ubuntu.setup.log:
	mkdir -p $(PROJECT_LOG_DIR)


ubuntu.setup.ulimit: config.files.generator
	ln -f -s $(PROJECT_DEPLOY)/limits.conf /etc/security/limits.d/$(PROJECT_FULL_NAME).conf
	@echo "System limits need a reboot before they become effective"

ubuntu.setup.systemd: venv config.files.generator ubuntu.setup.log ubuntu.fixperm
	cp $(PROJECT_DEPLOY)/systemd_celery_beat.service 	/etc/systemd/system/
	cp $(PROJECT_DEPLOY)/systemd_celery_worker.service  /etc/systemd/system/
	cp $(PROJECT_DEPLOY)/systemd_uwsgi.service 			/etc/systemd/system/

	systemctl daemon-reload
	systemctl --force enable  systemd_celery_beat.service systemd_celery_worker.service systemd_uwsgi.service
	systemctl --force restart systemd_celery_beat.service systemd_celery_worker.service systemd_uwsgi.service


ubuntu.fixperm: config.files.generator ubuntu.setup.log
	chown -R $(PROJECT_USER). $(PROJECT_ROOT)

db.mysql.install:
	apt install -y -qq libmariadb2 libmariadbd-dev libmysqlclient-dev mariadb-client mariadb-server

db.mysql.setup: db.mysql.install
	mysql -uroot <<MYSQL_SCRIPT
	create database $(DB_NAME) CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
	create user $(DB_USER);
	grant all on $(DB_NAME).* to '$(DB_USER)'@'localhost' identified by '$(DB_PASS)';
	FLUSH PRIVILEGES;
	MYSQL_SCRIPT

db.mysql.clean:
	mysql -uroot <<MYSQL_SCRIPT
	drop database $(DB_NAME);
	drop user $(DB_USER);
	FLUSH PRIVILEGES;
	MYSQL_SCRIPT


webserver.nginx.install:
	apt install nginx-extras -y

webserver.nginx.setup: webserver.nginx.install config.files.generator ubuntu.setup.log webserver.nginx.tls_dhparam webserver.nginx.tls_self_signed webserver.nginx.tls_csr ubuntu.fixperm
	ln -f -s $(PROJECT_DEPLOY)/nginx.conf /etc/nginx/sites-enabled/$(PROJECT_FULL_NAME)
	# to grant nginx process access to files in a directory owned by the user
	adduser www-data $(PROJECT_USER)
	# to allow web server to write into the application log directory
	chmod g+w $(PROJECT_LOG_DIR)
	# to trace and correct file permission issues
	namei -lnx $(PROJECT_ROOT)
	service nginx configtest
	service nginx restart


webserver.nginx.tls_dhparam:
	# to secure diffie-hellman, generate a stronger and unique 4096 bit long
	# DH GROUP using OpenSSL. This might take a few minutes.
	openssl dhparam -out $(PROJECT_DEPLOY)/dhparam.pem 1024
	chmod 640 $(PROJECT_DEPLOY)/dhparam.pem

webserver.nginx.tls_self_signed:
	@echo "OpenSSL will now ask you several questions. The only important
		field is Common Name, which should be set to the domain name
		or IP address of the server which will be accessed by clients.
		The other fields can be left at their defaults by just
		pressing enter or can be filled in with anything."
	# to generate a self-signed certificate
	openssl req -x509 -nodes -sha256 -days 365 -newkey rsa:2048 \
		-outform PEM \
		-keyout $(PROJECT_DEPLOY)/cert_self_signed_$(PROJECT_DOMAIN)_key.pem \
		-out $(PROJECT_DEPLOY)/cert_self_signed_$(PROJECT_DOMAIN).pem
	chmod 640 $(PROJECT_DEPLOY)/cert_self_signed_$(PROJECT_DOMAIN).pem

webserver.nginx.tls_csr:
	# to generate the CSR which is to be given to CA for signing
	openssl req -nodes -new -sha256 -newkey rsa:2048 \
		-outform PEM \
		-keyout $(PROJECT_DEPLOY)/cert_$(PROJECT_DOMAIN)_key.pem \
		-out $(PROJECT_DEPLOY)/cert_$(PROJECT_DOMAIN).csr
	chmod 640 $(PROJECT_DEPLOY)/cert_$(PROJECT_DOMAIN)_key.pem



webserver.apache.install:

webserver.apache.setup: webserver.apache.install
	a2enmod alias headers deflate macro rewrite ratelimit expires wsgi proxy
	ln -f -s $(PROJECT_DEPLOY)/apache.conf /etc/apache2/sites-available/$(PROJECT_FULL_NAME).conf
	a2ensite $(PROJECT_FULL_NAME).conf
	# to grant apache process access to files in a directory owned by the user
	adduser www-data $(PROJECT_USER)
	# to allow web server to write into the application log directory
	chmod g+w $(PROJECT_LOG_DIR)
	# to trace and correct file permission issues
	namei -lnx $(PROJECT_ROOT)
	service apache2 restart

broker.rabbitmq.install:
	apt install -y -qq rabbitmq-server

broker.rabbitmq.setup: broker.rabbitmq.install
	rabbitmq-plugins enable rabbitmq_management
	rabbitmqctl add_user $(RABBITMQ_USER) $(RABBITMQ_PASS)
	rabbitmqctl set_user_tags $(RABBITMQ_USER) administrator
	rabbitmqctl set_permissions -p / $(RABBITMQ_USER) ".*" ".*" ".*"


venv: $(PROJECT_VENV)/bin/activate

.ONESHELL:
$(PROJECT_VENV)/bin/activate:
	test -d $(PROJECT_VENV) || $(PYTHON) -m venv $(PROJECT_VENV)
	source $(PROJECT_VENV)/bin/activate
	pip3 install --no-cache-dir --upgrade --force-reinstall --compile pip
	pip3 install --no-cache-dir --upgrade --force-reinstall --compile --requirement requirements/$(PROJECT_DEPLOY_ENV).txt
	touch $(PROJECT_VENV)/bin/activate


.ONESHELL:
django.setup: django.migrate django.loaddata django.messages django.files

.ONESHELL:
django.files: venv
	source $(PROJECT_VENV)/bin/activate
	export DJANGO_SETTINGS_MODULE=$(PROJECT_NAME).settings.$(PROJECT_DEPLOY_ENV)
	cd $(PROJECT_SRC)
	$(PYTHON) manage.py collectstatic --noinput

.ONESHELL:
django.loaddata: venv
	source $(PROJECT_VENV)/bin/activate
	export DJANGO_SETTINGS_MODULE=$(PROJECT_NAME).settings.$(PROJECT_DEPLOY_ENV)
	cd $(PROJECT_SRC)
	$(PYTHON) manage.py loaddata --ignorenonexistent 	auth.user.json \
														django_celery_beat.crontabschedule.json \
														django_celery_beat.intervalschedule.json \
														django_celery_beat.periodictask.json \
														django_celery_beat.periodictasks.json \
														sites.site.json \
														collector.marketplacemodel.json \
														collector.symbolmodel.json

.ONESHELL:
django.messages: venv
	source $(PROJECT_VENV)/bin/activate
	export DJANGO_SETTINGS_MODULE=$(PROJECT_NAME).settings.$(PROJECT_DEPLOY_ENV)
	cd $(PROJECT_SRC)
	$(PYTHON) manage.py makemessages --locale fa --locale en --no-wrap  --verbosity 2
	$(PYTHON) manage.py compilemessages --locale fa --locale en --verbosity 2

.ONESHELL:
django.migrate: venv
	source $(PROJECT_VENV)/bin/activate
	export DJANGO_SETTINGS_MODULE=$(PROJECT_NAME).settings.$(PROJECT_DEPLOY_ENV)
	cd $(PROJECT_SRC)
	$(PYTHON) manage.py makemigrations --verbosity 2
	$(PYTHON) manage.py migrate --verbosity 2

.ONESHELL:
django.runserver: venv
	source $(PROJECT_VENV)/bin/activate
	export DJANGO_SETTINGS_MODULE=$(PROJECT_NAME).settings.$(PROJECT_DEPLOY_ENV)
	cd $(PROJECT_SRC)
	$(PYTHON) manage.py runserver --verbosity 2 localhost:$(PROJECT_PORT_UWSGI)

.ONESHELL:
django.uwsgi: venv
	source $(PROJECT_VENV)/bin/activate
	uwsgi --ini $(PROJECT_DEPLOY)/uwsgi.ini --http localhost:$(PROJECT_PORT_UWSGI)

.ONESHELL:
django.gunicorn: venv
	source $(PROJECT_VENV)/bin/activate
	gunicorn --workers=1 --bind=localhost:$(PROJECT_PORT_UWSGI) $(PROJECT_NAME).wsgi:application


.ONESHELL:
django.test: venv
	source $(PROJECT_VENV)/bin/activate
	export DJANGO_SETTINGS_MODULE=$(PROJECT_NAME).settings.$(PROJECT_DEPLOY_ENV)
	cd $(PROJECT_SRC)
	$(PYTHON) manage.py test

.ONESHELL:
django.check: venv
	source $(PROJECT_VENV)/bin/activate
	export DJANGO_SETTINGS_MODULE=$(PROJECT_NAME).settings.$(PROJECT_DEPLOY_ENV)
	cd $(PROJECT_SRC)
	$(PYTHON) manage.py check

.ONESHELL:
django.check.deploy: venv
	source $(PROJECT_VENV)/bin/activate
	export DJANGO_SETTINGS_MODULE=$(PROJECT_NAME).settings.$(PROJECT_DEPLOY_ENV)
	cd $(PROJECT_SRC)
	$(PYTHON) manage.py check --deploy

.ONESHELL:
django.celery.worker: venv
	source $(PROJECT_VENV)/bin/activate
	export DJANGO_SETTINGS_MODULE=$(PROJECT_NAME).settings.$(PROJECT_DEPLOY_ENV)
	cd $(PROJECT_SRC)
	celery worker --app $(PROJECT_NAME) --task-events --loglevel info --autoscale=30,0 --pidfile /tmp/celeryworker.pid


.ONESHELL:
django.celery.beat: venv
	source $(PROJECT_VENV)/bin/activate
	export DJANGO_SETTINGS_MODULE=$(PROJECT_NAME).settings.$(PROJECT_DEPLOY_ENV)
	cd $(PROJECT_SRC)
	celery beat   --app $(PROJECT_NAME) --loglevel debug --scheduler django_celery_beat.schedulers:DatabaseScheduler --pidfile /tmp/celerybeat.pid  --max-interval 300



.ONESHELL:
config.files.generator:
	@export PROJECT_NAME=$(PROJECT_NAME)
	export PROJECT_USER=$(PROJECT_USER)
	export PROJECT_DEPLOY_ENV=$(PROJECT_DEPLOY_ENV)
	export PROJECT_FULL_NAME=$(PROJECT_FULL_NAME)
	export PROJECT_DOMAIN=$(PROJECT_DOMAIN)
	export PROJECT_IPADDR=$(PROJECT_IPADDR)
	export PROJECT_PORT_HTTP=$(PROJECT_PORT_HTTP)
	export PROJECT_PORT_HTTPS=$(PROJECT_PORT_HTTPS)
	export PROJECT_PORT_UWSGI=$(PROJECT_PORT_UWSGI)
	export PROJECT_PORT_API=$(PROJECT_PORT_API)
	export PROJECT_ROOT=$(PROJECT_ROOT)
	export PROJECT_SRC_DIR=$(PROJECT_SRC_DIR)
	export PROJECT_SRC=$(PROJECT_SRC)
	export PROJECT_VENV_DIR=$(PROJECT_VENV_DIR)
	export PROJECT_VENV=$(PROJECT_VENV)
	export PROJECT_DEPLOY_DIR=$(PROJECT_DEPLOY_DIR)
	export PROJECT_DEPLOY=$(PROJECT_DEPLOY)
	export PROJECT_LOG_DIR=$(PROJECT_LOG_DIR)
	export PROJECT_TMP_DIR=$(PROJECT_TMP_DIR)
	export PROJECT_SPHINX_DIR=$(PROJECT_SPHINX_DIR)
	for template_file in $(PROJECT_DEPLOY)/tmpl/*; do \
	echo "Processing template file \"$$template_file\""; \
	perl -p -e 's/\$$\{\{\s*(\w+)\s*\}\}/defined $$ENV{$$1} ? $$ENV{$$1} : "<TEMPLATE_VAR_NOT_FOUND>"/eg' < $$template_file > $(PROJECT_DEPLOY)/$$(basename $$template_file); \
	done
	grep -qr "<TEMPLATE_VAR_NOT_FOUND>" $(PROJECT_DEPLOY) && echo "Warning: some template variables were not found" || true
